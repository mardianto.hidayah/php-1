<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";
        $string = "PHP is never old";
        $panjang = strlen($string);
        echo 'Kalimat pertama: '. $string . '<br>';
        echo 'Panjang string: '. $panjang . '<br>';
        echo 'Jumlah kata: '. str_word_count($string);
        /* 
            SOAL NTunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! 
            Tunjukkan juga jumlah kata di dalam kalimat tersebut! 

            Contoh: 
            $string = "PHP is never old";
            Output:
            Panjang string: 16, 
            Jumlah kata: 4 
        */

        $first_sentence = "Hello PHP!" ; // Panjang string 10, jumlah kata: 2
        $second_sentence = "I'm ready for the challenges"; // Panjang string: 28,  jumlah kata: 5
        
        echo "<h3> Soal No 2</h3>";
        echo 'Kalimat pertama: '. $first_sentence . '<br>';
        echo 'Kata pertama: '. substr($first_sentence,0,5) . '<br>';
        echo 'Kata kedua: '. substr($first_sentence,6,4) . '<br><br>';

        echo 'Kalimat kedua: '. $second_sentence . '<br>';
        echo 'Kata pertama: '. substr($second_sentence,0,3) . '<br>';
        echo 'Kata kedua: '. substr($second_sentence,4,5) . '<br>';
        echo 'Kata ketiga: '. substr($second_sentence,10,3) . '<br>';
        echo 'Kata keempat: '. substr($second_sentence,14,3) . '<br>';
        echo 'Kata kelima: '. substr($second_sentence,18,10) . '<br><br>';
        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        // Lanjutkan di bawah ini
        echo "Kata kedua: " . substr($string2, 2, 4);
        echo "<br> Kata Ketiga: " . substr($string2, 7, 3) . "<br>" ;

        echo "<h3> Soal No 3 </h3>";
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        $string3 = "PHP is old but Good!";
        echo "String: \"$string3\" <br>"; 
        $string4 = str_replace("Good", "Awesome",$string3);
        echo "String: \"$string4\" <br>"; 
        // OUTPUT : "PHP is old but awesome"

    ?>
</body>
</html>